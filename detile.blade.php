@extends('master')
@section('halaman')

tampil
@endsection


@section('content')
<div class="card border-success mb-3" style="max-width: 18rem;">
  <div class="card-header bg-transparent border-success">{{$data->nama }}</div>
  <div class="card-body text-success">
    <h5 class="card-title">{{$data->alamat }}</h5>
    <p class="card-text">{{$data->bio }}</p>
  </div>


  <div class="card-footer bg-transparent border-success">
<a href="/cast/{{ $data->id }}/edit">edit</a>
  </div>

</div>
@endsection
