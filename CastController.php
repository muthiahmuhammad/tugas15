<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function index()
    {
        $users = DB::table('cast')->get();

        return view('tampil', ['data' => $users]);
    }
    public function create()
    {
        return
            view('tambah');
    }
    public function store(request $request)
    {
        DB::table('cast')->insert([
            'nama' => $request["nama"],
            'alamat' => $request["alamat"],
            'bio' => $request["bio"]
        ]);
        $users = DB::table('cast')->get();

        return view('tampil', ['data' => $users]);


    }

    public function show($cast_id)
    {
        $affected = DB::table('cast')
            ->where('id', $cast_id)->first();

        return view('detile', ['data' => $affected]);
    }
    public function edit($cast_id)
    {
        $tes = DB::table('cast')
            ->where('id', $cast_id)->first();
        return view('edit', ['data' => $tes]);

    }
    public function update(request $request, $c)
    {
        DB::table('cast')
            ->where('id', $c)
            ->update([
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'bio' => $request->bio
            ]);
        return redirect('/cast');



    }
    public function destroy($cast_id)
    {
        DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast');
    }
}